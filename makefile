analyze: analyze.cpp semantics.h lex.yy.cc parse.cc
	g++ -o analyze analyze.cpp lex.yy.cc parse.cc

lex.yy.cc: symbols.l
	flex++ symbols.l

parse.cc: syntax.y
	bisonc++ syntax.y

clean:
	rm -f lex.yy.cc analyze
