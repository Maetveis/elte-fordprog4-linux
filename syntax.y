%baseclass-preinclude "semantics.h"
%lsp-needed

%union
{
	std::string* name;
	ExpressionSemantics* semantics;
}

%type <semantics> expression

%token INT
%token MAIN
%token UNSIGNED
%token BOOL
%token TRUE
%token FALSE
%token IF
%token ELSE
%token WHILE
%token COUT
%token CIN

%token NUMBER

%token POPEN
%token PCLOSE
%token BOPEN
%token BCLOSE

%token <name> ID

%token END

%token NOT

%left LET
%left STREAM_IN
%left STREAM_OUT
%left AND
%left OR
%left EQUAL
%left LT
%left GT
%left LE
%left GE
%left PLUS
%left MINUS
%left TIMES
%left DIV
%left MOD

%%

start:
	signiture BOPEN declaration_block commands BCLOSE
	{

	}
;
signiture:
	INT MAIN POPEN PCLOSE
	{

	}
;
declaration_block:
	declaration declaration_block
	{

	}
|
	//Empty
	{

	}
;
declaration:
	UNSIGNED ID END
	{
		if(variables.find(*$2) != variables.end())
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Variable \'" << *$2 << "\' redeclared" << '\n';
			exit(1);
		} else {
			variables[*$2] = ExpressionSemantics(d_loc_.first_line, ExpressionType::Unsigned);
		}
		delete $2;
	}
|
	BOOL ID END
	{
		if(variables.find(*$2) != variables.end())
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Variable " << *$2 << " doubly declared" << '\n';
			exit(1);
		} else {
			variables[*$2] = ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
		}
		delete $2;
	}
;
body:
	BOPEN commands BCLOSE
	{

	}
;
commands:
	command
	{

	}
|
	command commands
	{

	}
;
command:
	statement
	{

	}
|
	control
	{

	}
;
statement:
	CIN STREAM_IN ID END
	{
		auto it = variables.find(*$3);
		if(it == variables.end() || it->second.lineNo > d_loc_.first_line)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Variable \'" << *$3 << "\' not yet declared" << '\n';
			exit(1);
		}
	}
|
	COUT STREAM_OUT expression END
	{

	}
|
	ID LET expression END
	{
		auto it = variables.find(*$1);
		if(it == variables.end() || it->second.lineNo > d_loc_.first_line)
		{
			std::cerr << "Variable \'" << *$1 << "\' not yet declared" << '\n';
			exit(1);
		} else if (it->second.type != $3->type){
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Types don't match in assignment" << '\n';
		}
		delete $1;
		delete $3;
	}
;
control:
	IF POPEN expression PCLOSE body else_block
	{

	}
|
	WHILE POPEN expression PCLOSE body
	{

	}
;
else_block:
	//Empty
	{

	}
|
	ELSE body
	{

	}
;
expression:
	NUMBER
	{
		$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Unsigned);
	}
|
	TRUE
	{
		$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
	}
|
	FALSE
	{
		$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
	}
|
	ID
	{
		auto it = variables.find(*$1);
		if(it == variables.end() || it->second.lineNo > d_loc_.first_line)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Variable \'" << *$1 << "\' not yet declared" << '\n';
			exit(1);
		} else {
			$$ = new ExpressionSemantics(d_loc_.first_line, it->second.type);
		}
	}
|
	NOT expression
	{
		if($2->type != ExpressionType::Bool)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Negating an expression of non Bool type" << '\n';
			exit(1);
		} else {
			delete $2;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
		}
	}
|
	POPEN expression PCLOSE
	{
		$$ = new ExpressionSemantics(d_loc_.first_line, $2->type);
		delete $2;
	}
|
	expression PLUS expression
	{
		if($1->type != ExpressionType::Unsigned || $3->type != ExpressionType::Unsigned)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Addition involving an expression of non unsigned type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Unsigned);
		}
	}
|
	expression MINUS expression
	{
		if($1->type != ExpressionType::Unsigned || $3->type != ExpressionType::Unsigned)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Substraction involving an expression of non unsigned type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Unsigned);
		}
	}
|
	expression TIMES expression
	{
		if($1->type != ExpressionType::Unsigned || $3->type != ExpressionType::Unsigned)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Multiplication involving an expression of non unsigned type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Unsigned);
		}
	}
|
	expression DIV expression
	{
		if($1->type != ExpressionType::Unsigned || $3->type != ExpressionType::Unsigned)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Divison involving an expression of non unsigned type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Unsigned);
		}
	}
|
	expression MOD expression
	{
		if($1->type != ExpressionType::Unsigned || $3->type != ExpressionType::Unsigned)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Modulus involving an expression of non unsigned type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Unsigned);
		}
	}
|
	expression LT expression
	{
		if($1->type != ExpressionType::Unsigned || $3->type != ExpressionType::Unsigned)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": < involving an expression of non unsigned type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
		}
	}
|
	expression GT expression
	{
		if($1->type != ExpressionType::Unsigned || $3->type != ExpressionType::Unsigned)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": > involving an expression of non unsigned type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
		}
	}
|
	expression LE expression
	{
		if($1->type != ExpressionType::Unsigned || $3->type != ExpressionType::Unsigned)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": <= involving an expression of non unsigned type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
		}
	}
|
	expression GE expression
	{
		if($1->type != ExpressionType::Unsigned || $3->type != ExpressionType::Unsigned)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": >= involving an expression of non unsigned type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
		}
	}
|
	expression EQUAL expression
	{
		if($1->type != $3->type)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Types dont match in equality operator" << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
		}
	}
|
	expression AND expression
	{
		if($1->type != ExpressionType::Bool || $3->type != ExpressionType::Bool)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": And involving an expression of non boolean type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
		}
	}
|
	expression OR expression
	{
		if($1->type != ExpressionType::Bool || $3->type != ExpressionType::Bool)
		{
			std::cerr << "Semantic Error on line " << d_loc_.first_line << ": Or involving an expression of non boolean type." << '\n';
			exit(1);
		} else {
			delete $1;
			delete $3;
			$$ = new ExpressionSemantics(d_loc_.first_line, ExpressionType::Bool);
		}
	}
;
